#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

#define BUFFER 100


int main( void ){

	int fd;
	char mensaje[BUFFER];

	//Crea el FIFO
	if ( mkfifo( "tuberia", 0600 ) < 0 ) {
		perror( "No se puede crear el FIFO" );
		return( -1 );
	}

	//Abre el FIFO
	if ( ( fd = open( "tuberia", O_RDONLY ) ) < 0 ) {
		perror( "No puede abrirse el FIFO" );
		return( -1 );
	}

	//Lee el FIFO e imprime el mensaje por pantalla
	while ( read( fd, mensaje, sizeof( char ) * BUFFER ) > 0 ) {
		printf( "%s\n", mensaje );
	}

	//Cierra y elimina el FIFO
	close(fd);
	unlink("tuberia");

	return( 0 );
}

